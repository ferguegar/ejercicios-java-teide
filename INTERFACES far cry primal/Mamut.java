
package ejercicio_interfaces_farcryprimal;

public class Mamut implements Defensa {
    
    float vida;

    public Mamut() {
    }

    public Mamut(float vida) {
        this.vida = vida;
    }

    public float getVida() {
        return vida;
    }

    public void setVida(float vida) {
        this.vida = vida;
    }

    @Override
    public String toString() {
        return "Mamut{" + "vida=" + vida + '}';
    }
    
    @Override
    public String defenderse() {
        return "MAMUT: puede cargar y dar trompazo, cuidado con los colmillos";
    }
    
    
}
