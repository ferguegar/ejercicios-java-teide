
package ejercicio_interfaces_farcryprimal;

public class enemigosFarcryPrimal {

    public static void main(String[] args) {
        /*Caso 1: Lista de elementos de tipo Izila que ejecutan (TODOS) los métodos toString() y azul()*/

        Izila[] lista = {new Arquero(20, false, "Walter"), new Jefazo("Registra el cuerpo", 100)};
        System.out.println("IMPLEMENTAN TODOS LOS MÉTODOS TO STRING Y AZUL");
        System.out.println("----------------------------------------------");
        for (Izila elemento : lista) {

            System.out.println(elemento.toString());

            System.out.println(elemento.azul());

        }

        /*Caso 2: Lista de elementos de tipo Defensa que ejecutan (TODOS) los métodos 
        toString(), azul() y defenderse()*/
        Defensa[] lista2 = {new Arquero(20, false, "Jerry"), new Jefazo("Registra el cuerpo", 200), new Mamut(1000)};
        System.out.println("");
        System.out.println("IMPLEMENTAN TODOS LOS MÉTODOS TO STRING, AZUL Y DEFENSA");
        System.out.println("----------------------------------------------");
        for (Defensa elemento2 : lista2) {

            System.out.println(elemento2.toString());

            System.out.println(elemento2.defenderse());
            
            //Hacemos downcasting/conversión para que apunta a Izila en vez de a Defensa        
            if (elemento2 instanceof Izila) {
            System.out.println(((Izila) elemento2).azul());
            }

        }

    }

}
