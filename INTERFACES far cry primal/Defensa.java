package ejercicio_interfaces_farcryprimal;

public interface Defensa {
    
    public abstract String defenderse();
    
}