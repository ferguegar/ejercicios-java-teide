package ejercicio_interfaces_farcryprimal;

abstract class Izila implements Defensa {
    
    
    String nombre;
    
    public abstract String azul();     

    public Izila() {
    }

    public Izila(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Izila{" + "nombre=" + nombre + '}';
    }   
        
}




abstract class Debiles extends Izila {
    
    boolean huida;

    public Debiles() {
    }

    public Debiles(boolean huida) {
        this.huida = huida;
    }

    public Debiles(boolean huida, String nombre) {
        super(nombre);
        this.huida = huida;
    }

    public boolean isHuida() {
        return huida;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Debiles{" + "huida=" + huida + '}';
    }       
}




abstract class Fuertes extends Izila {
    
    float coraza;
    
    public Fuertes() {
    }
    
    public Fuertes(float coraza) {
        this.coraza = coraza;
    }

    public float getCoraza() {
        return coraza;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public void setCoraza(float coraza) {
        this.coraza = coraza;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Fuertes{" + "coraza=" + coraza + '}';
    }   
}
    



class Arquero extends Debiles {
    
    int disparo;
    
    @Override
    public String azul() {
        return "ARQUEROS: nos vestimos de azul";
    }
    
    @Override
    public String defenderse() {
        return "Atacaremos guardando las distancias";
    }
    
    public Arquero() {
    }

    public Arquero(int disparo) {
        this.disparo = disparo;
    }

    public Arquero(int disparo, boolean huida) {
        super(huida);
        this.disparo = disparo;
    }

    public Arquero(int disparo, boolean huida, String nombre) {
        super(huida, nombre);
        this.disparo = disparo;
    }

    public int getDisparo() {
        return disparo;
    }

    @Override
    public boolean isHuida() {
        return huida;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public void setDisparo(int disparo) {
        this.disparo = disparo;
    }

    public void setHuida(boolean huida) {
        this.huida = huida;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Arquero{" + "disparo=" + disparo + '}';
    }          
}




class Jefazo extends Fuertes {
    
    String mensaje;
    
    @Override
    public String azul() {
        return "JEFAZO: el azul es nuestro orgullo y lo llevamos por dentro";
    }
    
    @Override
    public String defenderse() {
        return "Nunca huimos y no será fácil acabar con nosotros";
    }

    public Jefazo() {
    }

    public Jefazo(String mensaje) {
        this.mensaje = mensaje;
    }

    public Jefazo(String mensaje, float coraza) {
        super(coraza);
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    @Override
    public float getCoraza() {
        return coraza;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setCoraza(float coraza) {
        this.coraza = coraza;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Jefazo{" + "mensaje=" + mensaje + '}';
    }   
}