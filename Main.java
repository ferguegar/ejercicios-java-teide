package entrega1_usuariosAnimales;

//import java.util.ArrayList;

/*Ejercicio 1: Crear una clase donde gestionar otros usuarios
Ejercicio 2: A�adir m�todo  mostrarTodosLosDatos() que muestre 
todos los campos del usuarios
Ejercicio 3: M�todo que reciba un String "mujer", "Mujer", 
"MUJER", "hombre", "hOMbre"...  y cambie el g�nero del usuario
Ejercicio 4: Nuevo constructor con todos los campos
Ejercicio 5: Nueva clase Animal con nombre, especie, genero, 
peso, edad. Y unos pocos m�todos y contructores que se ocurran, 
e instanciar 2 animales*/
public class Main {

	public static void main(String[] args) {
		
		Usuarios[] listaUsuarios = 		   	{new Usuarios("Fernando", "91-555 22 66", 29, 1433.33f, true, "HomBRE"), 
											new Usuarios("Crizzty", "+44-888 99 55", 26, 2000.50f, true, "MujeR"),
											new Usuarios("Mam�", "91-888 44 31", 62, 2500, true, "MujEr"),
											new Usuarios("Mr. Hermafrodita", "", 50, 1000, false, "Hermafrodita"),
											new Usuarios("Diego/Mayte", "0065-27-43 55 897", 20, 0, true, "Gender Fluid")};
		
		for (Usuarios usuario : listaUsuarios) {
			System.out.println(usuario.toString());
		}
		
		
		Animales[] listaAnimales = 			{new Animales("Isis", "perro", 'H', 20.25f, 13),
											new Animales("Tiro", "elefante", 'M', 350f, 12),
											new Animales("Cobarde", "galliNA", 'H', 1.75f, 0),
											new Animales("Ganya", "gato", 'H', 7.4f, 8),
							  		 		new Animales("Yambo", "conejo", 'H', 2.5f, 7),
							  		 		new Animales("ESPECIE DESCONOCIDA", "Cualquier COSA", 'O', 25f, 5)};
		
		for (Animales animal : listaAnimales) {
			System.out.println(animal.toString());
			Animales.rugir(animal);
		}
		
		
	}

}
